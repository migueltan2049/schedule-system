const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const CreateError = require("http-errors");
const ProductRouter = require("./routes/routes");
const UserRouter = require("./routes/routes");
const ConsultRouter = require("./routes/routes");
require("dotenv").config();

const app = express();

app.use(cors());

app.use(bodyParser.json());

app.use("/", ProductRouter);
app.use("/", UserRouter);
app.use("/consult", ConsultRouter);

app.use((req, res) => {
  res.send(CreateError(404));
});

module.exports = app;
