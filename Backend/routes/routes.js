const express = require("express");
//const ProductController = require("../controllers/product");
const UserController = require("../controllers/user");
const ConsultController = require("../controllers/consult");
const auth = require("../middleware/auth");
const router = express.Router();

// router.get("/", ProductController.getProducts);

// router.get("/:id", ProductController.findProduct);

// router.post("/", ProductController.createProduct);

// router.put("/:id", ProductController.updateProduct);

// router.delete("/:id", ProductController.deleteProduct);

//auth
router.post("/register", UserController.register);

router.post("/login", UserController.login);

router.post("/logout", UserController.logout);

router.get("/user", auth, UserController.user);

router.patch("/update", auth, UserController.updateUser);

//--

router.get("/", auth, ConsultController.getSched);

router.get("/sched/:id", auth, ConsultController.getSchedId);

router.post("/", auth, ConsultController.create);

router.delete("/:id", auth, ConsultController.delete);

module.exports = router;
