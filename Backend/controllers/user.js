const bcrypt = require("bcrypt");
const UserModel = require("../models/user");
const jwt = require("jsonwebtoken");

exports.register = async (req, res) => {
  const { fullname, email, password, college, course } = req.body;

  try {
    const emailExists = await UserModel.findOne({ where: { email } });
    if (emailExists) {
      return res.status(400).json({
        success: false,
        message: "Email already exists",
      });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const user = await UserModel.create({
      fullname,
      email,
      password: hashedPassword,
      college,
      course,
    });

    res.status(200).json({
      success: true,
      message: "User created successfully",
      data: user,
    });
  } catch (error) {
    res.status(500).send({
      success: false,
      message: "Internal Server Error.",
      error: error.message,
    });
  }
};

exports.login = async (req, res) => {
  const { email, password } = req.body;
  const user = await UserModel.findOne({ where: { email } });
  console.log(user);
  try {
    if (!email || !password) {
      v;
      return res.status(400).send({
        success: false,
        message: "Please provide the necessary fields.",
      });
    }

    if (!user) {
      return res.status(400).send({
        success: false,
        message: "Invalid Credentials.",
      });
    }

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(400).send({
        success: false,
        message: "Invalid Credentials.",
      });
    }
    const jwtToken = jwt.sign(
      { id: user.id, email: user.email },
      process.env.JWT_SECRET
    );
    console.log(jwtToken);

    res.status(200).send({
      token: jwtToken,
    });
  } catch (error) {
    res.status(500).send({
      success: false,
      message: "Internal Server Error.",
      error: error.message,
    });
  }
};

exports.user = async (req, res) => {
  try {
    const user = await UserModel.findByPk(req.userId);
    if (!user) {
      return res.status(404).send("User not found");
    }
    const email = user.email;
    const fullname = user.fullname;
    const college = user.college;
    const course = user.course;
    res.send({ email, fullname, college, course });
  } catch (error) {
    console.log(error);
  }
};

exports.logout = async (req, res) => {
  try {
    res.clearCookie("token");
    res.status(200).send({
      success: true,
      message: "Logout successful",
    });
  } catch (error) {
    res.status(500).send({
      success: false,
      message: "Internal Server Error.",
      error: error.message,
    });
  }
};

exports.updateUser = async (req, res) => {
  const { email, college, course, password } = req.body;
  const userId = req.userId;

  try {
    const user = await UserModel.findByPk(userId);
    if (!user) {
      return res.status(404).send("User not found");
    }

    if (email) {
      const emailExists = await UserModel.findOne({ where: { email } });
      if (emailExists && email !== user.email) {
        return res.status(400).json({
          success: false,
          message: "Email already exists",
        });
      }
      user.email = email;
    }

    if (college) {
      user.college = college;
    }

    if (course) {
      user.course = course;
    }

    if (password) {
      const hashedPassword = await bcrypt.hash(password, 10);
      user.password = hashedPassword;
    }

    await user.save();

    res.status(200).json({
      success: true,
      message: "User updated successfully",
      data: user,
    });
  } catch (error) {
    res.status(500).send({
      success: false,
      message: "Internal Server Error.",
      error: error.message,
    });
  }
};
