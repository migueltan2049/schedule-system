const ConsultModel = require("../models/consult");

exports.getSched = async (req, res) => {
  try {
    const consults = await ConsultModel.findAll();
    res.json(consults);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Unable to retrieve consultations." });
  }
};

exports.getSchedId = async (req, res) => {
  try {
    const consult = await ConsultModel.findByPk(req.params.id);
    if (!consult) {
      res.status(404).json({ error: "Consultation not found." });
    } else {
      res.json(consult);
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Unable to retrieve consultation." });
  }
};

exports.create = async (req, res) => {
  try {
    const consult = await ConsultModel.create(req.body);
    res.json(consult);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Unable to create consultation." });
  }
};

exports.delete = async (req, res) => {
  try {
    const deletedRows = await ConsultModel.destroy({
      where: { id: req.params.id },
    });
    if (deletedRows === 0) {
      res.status(404).json({ error: "Consultation not found." });
    } else {
      res.json({ success: true });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Unable to delete consultation." });
  }
};
