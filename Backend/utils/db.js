const Sequelize = require("sequelize");

const sequelize = new Sequelize({
  hostname: "localhost",
  database: "sample",
  username: "root",
  password: null,
  dialect: "mysql",
});

module.exports = sequelize;
