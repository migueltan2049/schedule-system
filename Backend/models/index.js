const sequelize = require("../utils/db");
//const ProductModel = require("./product");
const UserModel = require("./user");
const ConsultModel = require("./consult");

//ProductModel.sync();
UserModel.sync();
ConsultModel.sync();

module.exports = sequelize;
