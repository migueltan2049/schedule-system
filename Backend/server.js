const app = require("./app.js");
const http = require("http");
const server = http.createServer(app);
const models = require("./models");

models.sync().then(() => {
  const port = process.env.PORT;
  server.listen(port, () =>
    console.log("Server is running on port" + " " + port)
  );
});
